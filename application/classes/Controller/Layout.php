<?php defined('SYSPATH') or die('No direct script access.');
 
abstract class Controller_Layout extends Controller_Template 
{
	// Define the template to use
	public $template = 'template/layout';

	public function before()
	{
		parent::before();
 
    	//Disable auto rendering if requested using ajax
        if($this->request->is_ajax()) $this->auto_render = FALSE;

		// Page Title
		$this->template->title = 'FaceTweets';
		
		// Meta Tags
		$this->template->meta_tags = array();
		
		// Relational Links (other than stylesheets)
		$this->template->links = array();
		
		// Stylesheets
		$this->template->styles = array(
										'vendor/bootstrap/css/bootstrap.min.css',
										'vendor/font-awesome/css/font-awesome.min.css',
										'css/freelancer.css',
										'css/jquery.growl.css'
										);

		// Javascripts
		$this->template->scripts = array(
										'vendor/jquery/jquery.min.js',
										'vendor/bootstrap/js/bootstrap.min.js',
										'js/dropzone.min.js',
										'js/freelancer.js',
										'js/jquery.growl.js',
										'js/jquery.bootpag.min.js'
										);
	
		// No content by default
		$this->template->header 	= '';
		$this->template->content 	= '';
		$this->template->footer 	= '';
		$this->template->js_script 	= '';

	}
 
} // End Controller Layout