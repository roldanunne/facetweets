<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<!DOCTYPE html>

<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
		
		<title><?=$title?></title>

    	<!-- Load CSS -->
		<?php foreach ($styles as $item) : ?>
    		<link href="<?=URL::base()?>assets/<?=$item?>" rel="stylesheet">
		<?php endforeach; ?>
	
	</head>
	<body>

    	<!-- Navigation -->	
		<?php echo $header; ?>

    	<!-- Page Content -->    
	    <div id="content" class="content">
			<?php echo $content; ?>		
	    </div>

    	<!-- Footer -->	
		<?php echo $footer; ?>

    	<!-- Load Java Script -->
		<?php foreach ($scripts as $item) : ?>
    		<script src="<?=URL::base()?>assets/<?=$item?>"></script>
		<?php endforeach; ?>
		
    	<!-- Custom Script -->	
		<?php echo $js_script; ?>

	</body>
	
</html>
